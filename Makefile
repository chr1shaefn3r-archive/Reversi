# ---------------------------------
# Makefile for BabyDevelop projects
# ---------------------------------

CC          = g++
LD          = g++
QTDIR       = /usr/include/qt4
MOC         = moc
RM          = rm -f
RUN         = ./$(BIN)
CFLAGS      = -c -Wall -g -I$(QTDIR) -I$(QTDIR)/Qt -I$(QTDIR)/QtCore -I$(QTDIR)/QtGui
LDFLAGS     = -L/usr/lib
LIBS        = -lQtCore -lQtGui -lQt3Support

# ---------------------------------
OBJECTS     = main.o steuerung.o steuerung.moc.o oberflaeche.o oberflaeche.moc.o spieler.o spielfeld.o
BIN         = Steuerung
# ---------------------------------

#all: clean $(OBJECTS) link
all: $(OBJECTS) link run

clean:
	-$(RM) *.moc.cpp
	-$(RM) *.moc.o
	-$(RM) *.o
	-$(RM) $(BIN)

compile_all: $(OBJECTS)

link:
	#$(CC) $(OBJECTS) $(PROGRAMMFENSTER_OBJ) $(LDFLAGS) $(LIBS) -o $(BIN)
	$(CC) $(OBJECTS) $(LDFLAGS) $(LIBS) -o $(BIN)

.cpp.o:
	$(CC) $(CFLAGS) $<

%.moc.cpp: %.h
	$(MOC) -o $@ $<

%.moc.o: %.moc.cpp
	$(CC) $(CFLAGS) -o $@ $<

run:
	$(RUN)
