/* oberflaeche.cpp */

#include "oberflaeche.h"

#include <QTableWidgetItem>

/* Konstruktor */
Oberflaeche::Oberflaeche(Steuerung* pSteuerung, int spalten, int reihen, QWidget* elternObjekt) : QWidget(elternObjekt)
{
   dieSteuerung = pSteuerung;
   t = new QTableWidget(reihen, spalten, this);
   t->setGeometry(40,40,245,245);
   t->setRowCount(reihen);
   t->setColumnCount(spalten);

   t->verticalHeader()->hide();
   t->horizontalHeader()->hide();

   for(int z=0; z<spalten; z++)
      t->setColumnWidth(z, 30);
   for(int z=0; z<reihen; z++)
      t->setRowHeight(z, 30);
   connect(t, SIGNAL( cellClicked(int, int) ), SLOT( zelleKlick(int, int)));

   //Beschriftung X-Richtung
   for(i=0;i<8;i++)
   {
      txtBeschriftungX[i] = new QLabel(QString::number(i), this);
      txtBeschriftungX[i]->setGeometry(50+i*30,15,30,30);
      txtBeschriftungX[i]->show();
   }
   //Beschriftung Y-Richtung
   for(i=0;i<8;i++)
   {
      txtBeschriftungY[i] = new QLabel(QString::number(i), this);
      txtBeschriftungY[i]->setGeometry(20,42+i*30,30,30);
      txtBeschriftungY[i]->show();
   }

   tasteNeuesSpiel = new QPushButton("&Neues Spiel", this);
   tasteNeuesSpiel->setGeometry(300, 20, 140, 30);
   tasteNeuesSpiel->show();
   connect(tasteNeuesSpiel, SIGNAL( clicked() ), SLOT( tastendruckNeuesSpiel()));

   txtPunkte = new QLabel("Punkte:", this);
   txtPunkte->setGeometry(310,70,80,30);
   txtPunkte->show();

   lblPunkteA = new QLineEdit("Spieler A: 0", this);
   lblPunkteA->setGeometry(310, 100, 120, 30);
   lblPunkteA->setReadOnly(true);
   lblPunkteA->show();

   lblPunkteB = new QLineEdit("Spieler B: 0", this);
   lblPunkteB->setGeometry(310, 140, 120, 30);
   lblPunkteB->setReadOnly(true);
   lblPunkteB->show();

   lblInfoZeile = new QLineEdit("Spieler A am Zug", this);
   lblInfoZeile->setGeometry(25, 320, 242, 30);
   lblInfoZeile->setReadOnly(true);
   lblInfoZeile->show();
}

/* Destruktor */
Oberflaeche::~Oberflaeche()
{
}


/* zeigeInfo */
void Oberflaeche::zeigeInfo(QString text)
{
   lblInfoZeile->setText(text);
}

/* zeigePunkte */
void Oberflaeche::zeigePunkte(QString textA, QString textB)
{
   lblPunkteA->setText(textA);
   lblPunkteB->setText(textB);
}

void Oberflaeche::setzeText(int x, int y, QString zeichen)
{
   QTableWidgetItem *newItem = new QTableWidgetItem(zeichen);
   newItem->setFont(QFont("Times", 18, 75));
   newItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
   if(!zeichen.compare("A")) {
      newItem->setBackground(QBrush(QColor(Qt::black)));
      newItem->setForeground(QBrush(QColor(Qt::white)));
   } else if(!zeichen.compare("B")) {
      newItem->setBackground(QBrush(QColor(Qt::yellow)));
      newItem->setForeground(QBrush(QColor(Qt::white)));
   } else {
      newItem->setBackground(QBrush(QColor(Qt::white)));
   }
   t->setItem(y, x, newItem);
}

void Oberflaeche::setzeBild(int x, int y, QString z)
{
   if(!z.compare("A")) {
      QTableWidgetItem* i = new QTableWidgetItem(QIcon("steinB.xpm"), 0);
      stein.append(i);
      int n = stein.count();
      t->setItem( x, y, stein.at( n-1 ) );
   } else {
      QTableWidgetItem* i = new QTableWidgetItem(QIcon("steinA.xpm"), 0);
      stein.append(i);
      int n = stein.count();
      t->setItem( x, y, stein.at( n-1 ) );
   }
}

void Oberflaeche::zelleKlick(int reihe, int spalte)
{
   emit angeklicktZelle(spalte, reihe);
}

void Oberflaeche::tastendruckNeuesSpiel()
{
   emit neuesSpiel();
}
