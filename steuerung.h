/* steuerung.h */

#ifndef STEUERUNG_H
#define STEUERUNG_H

#include <qwidget.h>
#include "oberflaeche.h"
class Oberflaeche;
#include "spielfeld.h"
class SpielFeld;
#include "spieler.h"
class Spieler;

class Steuerung : public QWidget
{
   Q_OBJECT
public:
   Steuerung(QWidget* elternObjekt = 0);   /* Konstruktor */
  ~Steuerung(); /* Destruktor  */
private:
   Oberflaeche* dieOberflaeche;
   SpielFeld*   dasSpielFeld;
   Spieler*     derSpieler[2];
   int nrSpieler;
   bool spielEntschieden;

public:
   void bearbeiteMausClick(int x, int y, QString z);

private:
   void spieleZug(int x, int y, QString z);
   int gibNaechsteSpielerNr(int nrSpielerAktuell);
   void zeigeInfo();

public slots:
   void auswerten(int, int);
   void starteSpiel();
};

#endif
