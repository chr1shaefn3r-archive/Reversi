/* spielfeld.h */

#ifndef SPIELFELD_H
#define SPIELFELD_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "steuerung.h"
class Steuerung;
#include "oberflaeche.h"
class Oberflaeche;

#include <qtextstream.h>
using namespace std;

class SpielFeld
{
public:
   SpielFeld(Steuerung* pSteuerung, Oberflaeche* pOberflaeche);  /* Konstruktor */
  ~SpielFeld();  /* Destruktor  */
private:
   Steuerung* dieSteuerung;
   Oberflaeche* dieOberflaeche;
   QString spielStein[8][8];
public:
   void initSpielFeld();
   bool testeSpielZug(int x, int y, QString z);
   bool testeSpielZug(QString z);
   void aendereZeichen(int x, int y, int r, QString z);
   bool aendereZeichenOK(int x, int y, int r, QString z);
   int  berechnePunkte(QString z);
   void setzeZeichen(int x, int y, QString z);
   QString gebeZeichen(int x, int y);
private:
   bool neueKoordinaten(int& x, int& y, int r);
};

#endif
