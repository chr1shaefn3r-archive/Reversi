/* steuerung.cpp */

#include "steuerung.h"

/* Konstruktor */
Steuerung::Steuerung(QWidget* elternObjekt) : QWidget(elternObjekt)
{
   this->setWindowTitle("Reversi");

   dieOberflaeche = new Oberflaeche(this, 8, 8, this);
   dieOberflaeche->show();

   connect(dieOberflaeche, SIGNAL(angeklicktZelle(int, int)), SLOT( auswerten(int, int)));
   connect(dieOberflaeche, SIGNAL(neuesSpiel()), SLOT(starteSpiel()));

   dasSpielFeld = new SpielFeld(this, dieOberflaeche);

   derSpieler[0] = new Spieler(dasSpielFeld, "A", "Spieler A");
   derSpieler[1] = new Spieler(dasSpielFeld, "B", "Spieler B");

   nrSpieler = 0; // Spieler A: nrSpieler = 0
                  // Spieler B: nrSpieler = 1

   // Das Spiel ist noch nicht enschieden
   this->spielEntschieden = false;

   this->starteSpiel();
}

/* Destruktor */
Steuerung::~Steuerung()
{
}

/* bearbeiteMausClick */
void Steuerung::bearbeiteMausClick(int x, int y, QString z)
{
   qWarning("[*] Steuerung::bearbeiteMausClick(%d, %d, %s)",x,y,qPrintable(z));
   if(this->spielEntschieden) {
	qWarning("[*] Spiel bereits gewonnen -> nichts zu tun!");
	return;
   }
   QString aktuellesZeichen = derSpieler[nrSpieler]->gibZeichen();
   if(dasSpielFeld->testeSpielZug(x, y, aktuellesZeichen)) {
      qWarning("[*] dasSpielFeld->testeSpielZug(%d, %d, %s): true", x, y, qPrintable(aktuellesZeichen));
      this->spieleZug(x, y, aktuellesZeichen);
   } else {
      qWarning("[*] dasSpielFeld->testeSpielZug(%d, %d, %s): false", x, y, qPrintable(aktuellesZeichen));
      dieOberflaeche->zeigeInfo("Ungueltiger Zug! - "+derSpieler[nrSpieler]->gibName()+" am Zug");
      return;
   }
   nrSpieler = this->gibNaechsteSpielerNr(nrSpieler);
   aktuellesZeichen = derSpieler[nrSpieler]->gibZeichen();
   if(dasSpielFeld->testeSpielZug(aktuellesZeichen)) {
qWarning("[steuerung] dasSpielFeld->testeSpielZug(%s): true", qPrintable(aktuellesZeichen));
      // der nächste Spieler kann noch einen Zug machen:
      this->zeigeInfo();
   } else if(dasSpielFeld->testeSpielZug(derSpieler[this->gibNaechsteSpielerNr(nrSpieler)]->gibZeichen())) {
      // der übernächste Spieler (der Spieler, der den aktuellen Klick verursacht hat) könnte noch einen Zug machen
      dieOberflaeche->zeigeInfo(derSpieler[nrSpieler]->gibName()+" muss aussetzen!");
      // aktuelle Spielernummer wieder "zurücksetzen":
      nrSpieler = this->gibNaechsteSpielerNr(nrSpieler);
   } else {
qWarning("[steuerung] dasSpielFeld->testeSpielZug(%s): false", qPrintable(aktuellesZeichen));
      // der nächste Spieler kann schon keinen Zug mehr machen!
      if(derSpieler[0]->gibPunkte() < derSpieler[1]->gibPunkte()) {
         dieOberflaeche->zeigeInfo(derSpieler[1]->gibName()+" gewinnt!");
         this->spielEntschieden = true;
      } else if(derSpieler[0]->gibPunkte() > derSpieler[1]->gibPunkte()) {
         dieOberflaeche->zeigeInfo(derSpieler[0]->gibName()+" gewinnt!");
         this->spielEntschieden = true;
      } else {
         // Unentschieden:
         dieOberflaeche->zeigeInfo(derSpieler[0]->gibName()+" gewinnt!");
         this->spielEntschieden = true;
      }
   }
   dieOberflaeche->zeigePunkte(derSpieler[0]->gibName()+": "+QString::number(derSpieler[0]->gibPunkte()), derSpieler[1]->gibName()+": "+QString::number(derSpieler[1]->gibPunkte()));
}

/* spieleZug */
void Steuerung::spieleZug(int x, int y, QString z)
{
qWarning("[steuerung] Steuerung::spieleZug(%d, %d, %s)",x, y, qPrintable(z));
   dasSpielFeld->setzeZeichen(x, y, z);
   for(int r=0;r<=7;r++) {
      if(dasSpielFeld->aendereZeichenOK(x, y, r, z)) {
qWarning("[steuerung] in Richtung: %d aendereZeichenOK(): true", r);
         dasSpielFeld->aendereZeichen(x, y, r, z);
      } else {
         //nothing ...
      }
   }
}

/* zeigeInfo */
void Steuerung::zeigeInfo()
{
   dieOberflaeche->zeigeInfo(derSpieler[nrSpieler]->gibName()+" am Zug");
}

void Steuerung::auswerten(int row, int column)
{
   qWarning("[*] User klickt: x=%d y=%d",row,column);
   bearbeiteMausClick(row, column, dasSpielFeld->gebeZeichen(row, column));
}

void Steuerung::starteSpiel()
{
//   qWarning("[*] Steuerung::starteSpiel");
//   qWarning("[*] dasSpielFeld->initSpielFeld;");
   dasSpielFeld->initSpielFeld();
   this->nrSpieler = 0;
   this->spielEntschieden = false;

   dasSpielFeld->setzeZeichen(3, 3, derSpieler[0]->gibZeichen());
   dasSpielFeld->setzeZeichen(4, 4, derSpieler[0]->gibZeichen());
   dasSpielFeld->setzeZeichen(4, 3, derSpieler[1]->gibZeichen());
   dasSpielFeld->setzeZeichen(3, 4, derSpieler[1]->gibZeichen());

   dieOberflaeche->zeigeInfo("Spieler A am Zug");
   dieOberflaeche->zeigePunkte("Spieler A: 2", "Spieler B: 2");
}


/* gibNaechsteSpielerNr */
int Steuerung::gibNaechsteSpielerNr(int nrSpielerAktuell)
{
   if(nrSpielerAktuell) {
      return 0;
   } else {
      return 1;
   }
}
