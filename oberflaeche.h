/* oberflaeche.h */

#ifndef OBERFLAECHE_H
#define OBERFLAECHE_H

#include <qwidget.h>
#include <QHeaderView> //Header fuer Spalten-/Zeilenkoepfe entfernen koennen
#include <QStringList>
#include <QTableWidget>
#include <qicon.h>
#include <qlist.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlineedit.h>

#include "steuerung.h"
class Steuerung;

class Oberflaeche : public QWidget
{
   Q_OBJECT
public:
   Oberflaeche(Steuerung*,int, int, QWidget *elternObjekt = 0);   /* Konstruktor */
  ~Oberflaeche();   /* Destruktor  */

private:
   Steuerung* dieSteuerung;
   QTableWidget *t;
   QList<QTableWidgetItem*> stein;

   QPushButton* tasteNeuesSpiel;
   QLabel*      txtPunkte;
   QLabel*      txtBeschriftungX[8];
   QLabel*      txtBeschriftungY[8];
   QLineEdit*   lblPunkteA;
   QLineEdit*   lblPunkteB;
   QLineEdit*   lblInfoZeile;

   int i; //Schleifenindex

public:
   void zeigeInfo(QString text);
   void zeigePunkte(QString text1, QString text2);

public slots:
   void setzeText(int, int, QString);
   void setzeBild(int, int, QString);

private slots:
   void zelleKlick(int, int);
   void tastendruckNeuesSpiel();

signals:
   void neuesSpiel();
   void angeklicktZelle(int, int);
};

#endif
