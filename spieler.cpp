/* spieler.cpp */

#include "spieler.h"

/* Konstruktor */
Spieler::Spieler(SpielFeld* pSpielFeld, QString pZeichen, QString pName)
{
   dasSpielFeld = pSpielFeld;
   derName = pName;
   dasZeichen = pZeichen;
}

/* Destruktor */
Spieler::~Spieler()
{
}

/* gibPunkte */
int Spieler::gibPunkte()
{
   return dasSpielFeld->berechnePunkte(dasZeichen);
}

/* gibZeichen */
QString Spieler::gibZeichen()
{
   return dasZeichen;
}

/* gibName */
QString Spieler::gibName()
{
   return derName;
}
