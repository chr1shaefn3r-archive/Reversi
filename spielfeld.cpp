/* spielfeld.cpp */

#include "spielfeld.h"

/* Konstruktor */
SpielFeld::SpielFeld(Steuerung* pSteuerung, Oberflaeche* pOberflaeche)
{
	dieSteuerung = pSteuerung;
	dieOberflaeche = pOberflaeche;
}

/* Destruktor */
SpielFeld::~SpielFeld()
{
}

/* initSpielFeld */
void SpielFeld::initSpielFeld()
{
//   qWarning("[*] SpielFeld::initSpielFeld");
   for(int i=0;i<8;i++) {
      for(int j=0;j<8;j++) {
         spielStein[i][j] = "U";
         dieOberflaeche->setzeText(i, j, "");
      }
   }
}

/* testeSpielZug */
bool SpielFeld::testeSpielZug(int x, int y, QString z)
{
//qWarning("SpielFeld::testeSpielZug(%d, %d, %s)", x, y, qPrintable(z));
   int tempX = x, tempY = y, r = 0;
   bool flag = true;
   if(!spielStein[x][y].compare("A")) {
      return false;
   } else if(!spielStein[x][y].compare("B")) {
      return false;
   } else {
      for(r=0;r<8;r++) {
         tempX = x; tempY = y;
//qWarning("---------------------------------------------------------------------");
//qWarning("%d. Durchlauf Startpunkt: %d/%d mit Richtung=%d", r, tempX, tempY, r);
         this->neueKoordinaten(tempX, tempY, r);
         if(!spielStein[tempX][tempY].compare(z) || !spielStein[tempX][tempY].compare("U")) {
//qWarning("z=%s / r=%d ist spielStein[%d][%d]=%s => mein eigener oder keiner! => naechste Runde", qPrintable(z), r, tempX, tempY, qPrintable(spielStein[tempX][tempY]));
            continue;
         }
         while(1) {
            if(this->neueKoordinaten(tempX, tempY, r)) {
               if(!spielStein[tempX][tempY].compare(z)) {
//qWarning("An der Stelle %d/%d ist wieder ein eigener => richtiger Zug => return true", tempX, tempY);
                  return true;
               } else if(!spielStein[tempX][tempY].compare("U")) {
//qWarning("An der Stelle %d/%d ist ein leeres Feld => naechste Richtung probieren => break", tempX, tempY);
                  break;
               } else if(spielStein[tempX][tempY].compare(z) && spielStein[tempX][tempY].compare("U")) {
//qWarning("Fail bei: %d/%d / r = %d / z = %s / spielStein[%d][%d]=%s", tempX, tempY, r, qPrintable(z), tempX, tempY, qPrintable(spielStein[tempX][tempY]));
                  continue;
               }
            } else {
               break;
            }
         }
      }
   }
//qWarning("in keine Richtung wurde ein Richtiger Zug festgestellt => return false");
   return false;
}

/* testeSpielZug */
bool SpielFeld::testeSpielZug(QString z)
{
qWarning("SpielFeld::testeSpielZug(%s)",qPrintable(z));
   for(int i=0;i<8;i++) {
      for(int j=0;j<8;j++) {
         if(!spielStein[i][j].compare("U")) {
            // Wenn der Spielstein nicht leer ist:
            if(this->testeSpielZug(i, j, z)) {
               return true;
            }
         }
      }
   }
   return false;
}

/* aendereZeichen */
void SpielFeld::aendereZeichen(int x, int y, int r, QString z)
{
qWarning("[SpielFeld] SpielFeld::aendereZeichen(%d, %d, %d, %s)", x, y, r, qPrintable(z));
   int zielX = x; int zielY = y;
   int schritte = 0;
   // Analysieren
   while(1) {
      if(this->neueKoordinaten(zielX, zielY, r)) {
         if(!spielStein[zielX][zielY].compare(z)) {
qWarning("[SpielFeld] spielStein[%d][%d] ist ein eigener / inzwischen %d Schritte", zielX, zielY, schritte);
            break;
         } else {
qWarning("[SpielFeld] spielStein[%d][%d] ist kein eigener! / inzwischen %d Schritte", zielX, zielY, schritte);
            schritte++;
         }
      }
   }
   // Ersetzen
   int tempX = x; int tempY = y;
   for(int i=0;i<=schritte;i++) {
qWarning("[SpielFeld] %d. Runde:",i);
      this->neueKoordinaten(tempX, tempY, r);
      this->setzeZeichen(tempX, tempY, z);
qWarning("Zelle %d/%d, durch %s ersetzt, restliche Schritte: %d", tempX, tempY, qPrintable(z), schritte-i);
qWarning("---------------------------------------------");
   }
}

/* andereZeichenOK */
bool SpielFeld::aendereZeichenOK(int x, int y, int r, QString z)
{
qWarning("[SpielFeld] SpielFeld::aendereZeichenOK(%d, %d, %d, %s)", x, y, r, qPrintable(z));
   int tempX = x, tempY = y;
   if(!(this->neueKoordinaten(tempX, tempY, r))) {
qWarning("[SpielFeld] neueKoordinaten(%d, %d, %d): false", tempX, tempY, r);
      return false;
   }
   if(!spielStein[tempX][tempY].compare(z) || !spielStein[tempX][tempY].compare("U")) {
qWarning("[SpielFeld] Spielstein[%d][%d] ist ein eigener oder keiner", tempX, tempY);
      return false;
   }
   // Umschließt dieser Zug tatsächlich gegnerische Steine?
   // Suche beginnt ab dem Stein nach dem den wir schon gefunden haben
   while(1) {
      if(this->neueKoordinaten(tempX, tempY, r)) {
         if(!spielStein[tempX][tempY].compare("U")) {
            return false;
         } else if(!spielStein[tempX][tempY].compare(z)) {
            return true;
//         } else if(spielStein[tempX][tempY].compare(z) && spielStein[tempX][tempY].compare("U")) {
         } else {
            continue;
         }
      } else {
         break;
      }
   }
qWarning("[SpielFeld] return false ganz am Ende :)");
   return false;
}

/* berechnePunkte */
int SpielFeld::berechnePunkte(QString z)
{
   int punkte = 0;
   for(int i=0;i<8;i++) {
      for(int j=0;j<8;j++) {
         if(!spielStein[i][j].compare(z)) {
            punkte++;
         }
      }
   }
   return punkte;
}

/* setzeZeichen */
void SpielFeld::setzeZeichen(int x, int y, QString z)
{
//qWarning("[*] SpielFeld::setzeZeichen(%d, %d, %s)",x,y, qPrintable(z));
   spielStein[x][y] = z;
   dieOberflaeche->setzeText(x, y, z);
}

/* gebeZeichen */
QString SpielFeld::gebeZeichen(int x, int y)
{
   return spielStein[x][y];
}

/* neueKoordinaten */
bool SpielFeld::neueKoordinaten(int& x, int& y, int r)
{
   int tx = x, ty = y;
   switch(r) {
   case 0:
      tx++; break;
   case 1:
      tx++;ty--; break;
   case 2:
      ty--; break;
   case 3:
      tx--;ty--; break;
   case 4:
      tx--; break;
   case 5:
      tx--;ty++; break;
   case 6:
      ty++; break;
   case 7:
      tx++;ty++; break;
   }
   if(tx >= 0 && tx <= 7 && ty >= 0 && ty <= 7) {
      x = tx;
      y = ty;
      return true;
   } else {
      return false;
   }
//      2
//    3 ^ 1
//     \|/
// 4 <- A -> 0
//     /|\
//    5 v 7
//      6
}
