/* spieler.h */

#ifndef SPIELER_H
#define SPIELER_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "spielfeld.h"
class SpielFeld;

#include <qtextstream.h>
using namespace std;

class Spieler
{
public:
   Spieler(SpielFeld* pSpielFeld, QString pZeichen, QString pName);  /* Konstruktor */
  ~Spieler();  /* Destruktor  */
private:
   SpielFeld* dasSpielFeld;
   QString derName;
   QString dasZeichen;
public:
   int gibPunkte();
   QString gibZeichen();
   QString gibName();
};

#endif
